const BinaryFile = require('binary-file');
// структура для записи
const fileStrictureWrite = [
  {type: "double", name: "testDouble", value: 0.12},
  {type: "int", name: "testInt", value: 1222},
  {type: "float", name: "testFloat", value: 0.222}
];
// структура чтения
const fileStrictureRead = [
  {type: "double", name: "testDouble", value: ""},
  {type: "int", name: "testInt", value: ""},
  {type: "float", name: "testFloat", value: ""}
];

const myBinaryFile = new BinaryFile('./file.bin', 'r+');

// асинхронная запись по структуре для записи (создание файла)
const asyncWrite = async () => {
  try {
    await myBinaryFile.open();
    console.log('File opened for write');
    for (let obj of fileStrictureWrite){
      switch (obj.type) {
        case "double":
          await myBinaryFile.writeDouble(obj.value);
          break;
        case "int":
          await myBinaryFile.writeInt32(obj.value);
          break;
        case "float":
          await myBinaryFile.writeFloat(obj.value);
          break;
      }
    }
    await myBinaryFile.close();
    console.log('File closed for write');
  } catch (err) {
    console.log(`There was an error: ${err}`);
  }
};

// асинхронное чтение по структуре для чтения
const asyncRead = async () => {
  try {
    await myBinaryFile.open();
    await myBinaryFile.seek(0);
    console.log('File opened for read');
    let length = 0;
    for (let obj of fileStrictureRead){
      switch (obj.type) {
        case "double":
          obj.value = await myBinaryFile.readDouble();
          break;
        case "int":
          obj.value = await myBinaryFile.readInt32();
          break;
        case "float":
          obj.value = await myBinaryFile.readFloat();
          break;
      }
    }
    await myBinaryFile.close().then( r => console.log('текущая структура для чтения (с данными)', fileStrictureRead));
    console.log('File closed for read');
  } catch (err) {
    console.log(`There was an error: ${err}`);
  }
};
// записываем в файл, после окончания записи - читаем его
asyncWrite().then( () => asyncRead());
